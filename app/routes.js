const { exchangeRates } = require('../src/util.js');
const express = require("express");
const router = express.Router();

	router.get('/rates', (req, res) => {
		return res.status(200).send(exchangeRates);
	})

	router.post('/currency', (req, res) => {

		//#1 & #2
		if(!req.body.hasOwnProperty('name')){
			return res.status(400).send({
				'error': "Bad Request: Missing required parameter NAME"
			})
		}

		//#3
		if(typeof req.body.name != 'string'){
			return res.status(400).send({
				'error': "Bad Request: Parameter name input is not a string"
			})
		}

		//#4
		if(req.body.name = null){
			return res.status(400).send({
				'error': "Bad Request: Parameter name is empty"
			})
		}

		//#5
		if(!req.body.hasOwnProperty('ex')){
			return res.status(400).send({
				'error': "Bad Request: Missing required parameter EX"
			})
		}

		//#6 
		if(typeof req.body.ex != 'object'){
			return res.status(400).send({
				'error': "Bad Request: Parameter ex is not an object"
			})
		}

		//#7
		if(Object.entries(req.body.ex).length === 0){
			return res.status(400).send({
				'error': "Bad Request: Parameter ex is empty"
			})
		}

		//#8
		if(!req.body.hasOwnProperty('alias')){
			return res.status(400).send({
				'error': "Bad Request: Parameter alias is missing"
			})
		}

		//#9
		if(typeof req.body.alias != 'string'){
			return res.status(400).send({
				'error': "Bad Request: Parameter alias is not a string"
			})
		}

		//#10
		if(req.body.alias == null){
			return res.status(400).send({
				'error': "Bad Request: Parameter alias is empty"
			})
		}

		//#11
		let foundExchangeRateAlias = exchangeRates.find((exchangeRate) => {

			return exchangeRate.alias === req.body.alias 
		})


		if(foundExchangeRateAlias){
			return res.status(400).send({
				'error': "Bad Request: Parameters should not have duplicates"
			})
		} 

		return res.status(200).send({
				'success': "Thank you"
			})
		
	

		
		//#12
		// let foundExchangeRate = exchangeRates.find((exchangeRate) => {

		// 	return exchangeRate.alias === req.body.alias && exchangeRate.name === req.body.name && exchangeRate.ex === req.body.ex
		// })
		
		

})
		

module.exports = router;
